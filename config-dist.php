<?php
// HTTP
define('HTTP_SERVER', 'http://tiroki.web.dev/');

// HTTPS
define('HTTPS_SERVER', 'http://tiroki.web.dev/');

// DIR
define('DIR_APPLICATION', 'C:/web_serv/OpenServer/domains/tiroki.web.dev/catalog/');
define('DIR_SYSTEM', 'C:/web_serv/OpenServer/domains/tiroki.web.dev/system/');
define('DIR_LANGUAGE', 'C:/web_serv/OpenServer/domains/tiroki.web.dev/catalog/language/');
define('DIR_TEMPLATE', 'C:/web_serv/OpenServer/domains/tiroki.web.dev/catalog/view/theme/');
define('DIR_CONFIG', 'C:/web_serv/OpenServer/domains/tiroki.web.dev/system/config/');
define('DIR_IMAGE', 'C:/web_serv/OpenServer/domains/tiroki.web.dev/image/');
define('DIR_CACHE', 'C:/web_serv/OpenServer/domains/tiroki.web.dev/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/web_serv/OpenServer/domains/tiroki.web.dev/system/storage/download/');
define('DIR_LOGS', 'C:/web_serv/OpenServer/domains/tiroki.web.dev/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/web_serv/OpenServer/domains/tiroki.web.dev/system/storage/modification/');
define('DIR_UPLOAD', 'C:/web_serv/OpenServer/domains/tiroki.web.dev/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'tiroki');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
